# KtCommon Library


## Summary

KtCommon is a library of utility classes.

## Installation

Add this line to your Gemfile:

    gem 'ktcommon'

And then execute:

    $ bundle

or install it yourself as:

    $ gem install ktcommon

## Usage

Require and instantiate classes as needed.

## Testing

KtCommon uses RSpec for testing.

To run all existing tests:

    $ rake spec

or directly:

    $ bundle exec rspec

## TODO

* Rename methods to follow ruby naming conventions (ie. `snake_case` rather than `camelCase`)

## Contributing

1. Fork it ( https://bitbucket.org/ktechsystems/ktcommon/fork )
1. Clone it (`git clone git@bitbucket.org:[my-bitbucket-username]/ktcommon.git`)
2. Create your feature branch (`git checkout -b my-new-feature`)
3. Create tests for your feature branch
4. Commit your changes (`git commit -am 'Add some feature'`)
5. Push to the branch (`git push origin my-new-feature`)
6. Create a new Pull Request

## License

This gem is licensed under the MIT license. See LICENSE file for details.

