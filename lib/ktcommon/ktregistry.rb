##############################################################################
# File:: ktRegistry.rb
# Purpose: Class to read windows registry.
# 
# Author::    Jeff McAffee 10/04/09
# Copyright:: Copyright (c) 2009 kTech Systems LLC. All rights reserved.
# Website::   http://ktechsystems.com
##############################################################################


require 'win32/registry'

class KtRegistry

  attr_accessor :coName
  attr_accessor :appName
  
  def initialize(companyName="kTechSystems")
    $LOG.debug "KtRegistry::initialize()"
    
    @coName = companyName
    @appName = "Unknown"
  end
  
  
  def softwarePath()
    path = 'Software' + "\\" + @coName + "\\" + @appName
  end
  
  
  def readValue(valueName, defaultValue)
    $LOG.debug "KtRegistry::readValue( #{valueName}, #{defaultValue} )"
    result = ""
    regType = nil
    regValue = nil
    
    begin
      Win32::Registry::HKEY_LOCAL_MACHINE.open(softwarePath()) do |reg|
        #default_reg_typ, reg_defaultVal = reg.read('')   # Throws error if value doesn't exist.
        regType, regValue = reg.read(valueName)
        if(defaultValue.class == TrueClass || defaultValue.class == FalseClass) # Handle returning bools.
          if(regValue != 0)
            regValue = true
          else
            regValue = false
          end
        end
      end
    rescue Win32::Registry::Error => e
      $LOG.error "Error ocurred while reading path: #{softwarePath()}, #{valueName}"
      $LOG.error "#{e.class} : #{e.to_s}"
      $LOG.debug "Returning user supplied default value."
      regValue = defaultValue
    end

    regValue
  end
  
  
  def writeValue(valueName, value)
    $LOG.debug "KtRegistry::setValue( #{valueName}, #{value} )"
    $LOG.debug "Value class: #{value.class}"
    regType = nil
    regValue = nil
    
    begin
      Win32::Registry::HKEY_LOCAL_MACHINE.open(softwarePath(), Win32::Registry::Constants::KEY_WRITE) do |reg|
        case value.class.to_s
          when 'Fixnum'
            # Write as a dword
            $LOG.debug "Writing integer to registry"
            reg.write_i(valueName, value)
          
          when 'TrueClass'
            # Write as a dword
            $LOG.debug "Writing TrueClass to registry"
            reg.write_i(valueName, 1)
          
          when 'FalseClass'
            # Write as a dword
            $LOG.debug "Writing FalseClass to registry"
            reg.write_i(valueName, 0)
          
          else
            # Write as a string
            $LOG.debug "Writing String to registry"
            reg.write_s(valueName, value)
          
        end
      end
    rescue Win32::Registry::Error => e
      $LOG.error "Error ocurred while writing path: #{softwarePath()}, #{valueName}"
      $LOG.error "#{e.class} : #{e.to_s}"
      return false
    end
    
    return true
  end
  
  
  def deleteValue(valueName)
    $LOG.debug "KtRegistry::deleteValue( #{valueName} )"
    begin
      Win32::Registry::HKEY_LOCAL_MACHINE.open(softwarePath(), Win32::Registry::Constants::KEY_WRITE) do |reg|
        reg.delete_value(valueName)
      end
    rescue Win32::Registry::Error => e
      $LOG.error "Error ocurred while deleting value (#{valueName}) from path: #{softwarePath()}"
      $LOG.error "#{e.class} : #{e.to_s}"
      return false
    end
    
    return true
  end
  
end # class KtRegistry


