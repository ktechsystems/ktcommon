##############################################################################
# File:: ktcsvloader.rb
# Purpose:: CSV Loader object for loading and parsing CSV files
# 
# Author::    Jeff McAffee 09/25/2010
# Copyright:: Copyright (c) 2010, kTech Systems LLC. All rights reserved.
# Website::   http://ktechsystems.com
#
# This class is based on an original implementation of
# ruby/gdl/ppmgenerator/lib/ppmgenerator/csvloader.rb
#
##############################################################################

require 'csv'

##############################################################################
# 
module KtCommon
	  
	###
	# CsvLoader is used to parse CSV data. It generates symbols to use as
	# hash keys based on the header row of data. This means that a header
	# row is REQUIRED. 
	#
	# - All loaded data can be retrieved from the .rows attribute.
	# - .rows is an array of Hashes, each hash being data for one row,
	#	indexed by the symbols mentioned above.
	#
	# Example Rows Data Layout:
	#
	# rows[0] = Hash{:colHeaderItem1 => "row1Col1 Data", :colHeaderItem2 => "row1Col2 Data"}
	# rows[1] = Hash{:colHeaderItem1 => "row2Col1 Data", :colHeaderItem2 => "row2Col2 Data"}
	# rows[2] = Hash{:colHeaderItem1 => "row3Col1 Data", :colHeaderItem2 => "row3Col2 Data"}
	#
	class CsvLoader

	attr_reader :verbose
	attr_reader :rows
		
		
		###
		# CsvLoader constructor
		#
		def initialize()
			$LOG.debug "KtCommon::CsvLoader::initialize"
			@verbose 	= false
			@rows = Array.new
		end
		  

		###
		# Turn on verbose messaging.
		#
		# arg:: true to turn on verbose messaging
		#
		def verbose(arg)
			$LOG.debug "KtCommon::CsvLoader::verbose( #{arg} )"
			puts "Verbose mode: #{arg.to_s}" if @verbose
			@verbose = arg
		end
		  
	  
		###
		# Create a symbol to be used for hash keys (column) indexes for 
		# each row. The symbol is created by removing all whitespace.
		# All capitalization will be kept as-is.
		#
		# hdr:: Header data row from loaded CSV file
		#
		def createSymbolsFromHeader(hdr)
			i = 0
			@header = Array.new
			hdr.each do |colHdr|
				#puts "colHdr (before): " + colHdr
				val = colHdr.gsub(/\s/, "").to_s
				#puts "colHdr (after):  " + val
				raise "Unrecognized header value: #{colHdr} in #{__FILE__}:#{__LINE__}" if !colHdr.nil? && val.nil?
				unless(colHdr.nil? || colHdr.empty?)
					#@header[i] = colHdr.gsub!(/s/, "").to_s.intern if(!colHdr.nil? && !colHdr.empty?)
					@header[i] = val.intern
				end
				i += 1
			end # each colHdr
			
			#puts "header[#{i.to_s}]: #{@header[i]}"
			
			return true
		end
		
		
		###
		# Process a row of data. Used internally.
		#
		# row:: a row of data read from a CSV file
		#
		def process(row)
			$LOG.debug "KtCommon::CsvLoader::process"
			rowData = Hash.new
			row.each_index do |i|
				rowData[@header[i]] = row[i] if(!row[i].nil? && !row[i].empty?)
			end
			@rows << rowData
		end
		private :process
		
		
		###
		# Load a CSV file and process it into an array of hashes.
		#
		# filepath:: path to file to process
		#
		def load(filepath)
			$LOG.debug "KtCommon::CsvLoader::load( #{filepath} )"
			
			if(!File.exist?( filepath ))
				raise IOError.new("File not found: #{filepath}")
				return false
			end
			
			reader = CSV.open(filepath, "r")
			header = reader.shift
			
			if(@verbose)
				puts "CSV Column Headers:"
				header.each {|h| puts "\t#{h}" }
				puts
			end
			
			if(!createSymbolsFromHeader( header ))
				return false
			end
			
			reader.each do |row|
				process(row)
			end
			
			return true
		end
		  
	  
		###
		# Generate an example CSV file.
		# filepath:: path and name of file to generate
		# hdr:: array of text to create headers from
		# rows:: an array of arrays of row data ( [ ["row1Col1", "row1Col2", "row1Col3"], ["row2Col1", "row2Col2", "row2Col3"] ] )
		#
		def generateTemplate(filepath, hdr, rows=nil)
			$LOG.debug "KtCommon::CsvLoader::generateTemplate( #{filepath}, hdr )"
		
			writer = CSV.open(filepath, "w")
			writer << hdr
			unless(rows.nil?)
				rows.each {|r| writer << r}
			end
			writer.close
		end
		
		
	end # class CsvLoader


end # module KtCommon
