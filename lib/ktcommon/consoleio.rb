##############################################################################
# File:: 	ktconsoleio.rb
# Purpose:: KtConsole I/O object gets input from the console.
# 
# Author::    Jeff McAffee 09/26/2010
# Copyright:: Copyright (c) 2010, kTech Systems LLC. All rights reserved.
# Website::   http://ktechsystems.com
##############################################################################


##############################################################################
# 
module KtCommon

require 'ktcommon/ktcmdline'
	  
	###
	# The ConsoleIO class provides methods for reading from and writing to
	# the console.
	#
	class ConsoleIO
	include KtCmdLine			# Mixin the getInput() method

	attr_reader			:answers
	
	
		###
		# ConsoleIO class constructor
		#
		def initialize()
			$LOG.debug "ConsoleIO::new"
			@answers = {}
		end
		
		
		###
		# Ask questions and store the answers.
		#	Questions will be an array of hashes. Each element in the array
		#	is a question. Each question hash can have the following keys:
		#		- :param	- this will be used as the key to the answer hash
		#		- :q		- The question to be displayed. No punctuation will be added so include it if needed.
		#		- :desc		- Description text to be displayed before showing the question (optional)
		#
		def ask(questions)
			$LOG.debug "ConsoleIO::ask"
			raise ArgumentError.new("Expecting Array of Hashes. [questions] not an Array.") if questions.class != Array
			raise ArgumentError.new("Expecting Array of Hashes. [questions] is empty.") if questions.empty?
			
			qs = []
			questions.each do |ques|
				raise ArgumentError.new("Expecting Hash.") if ques.class != Hash
				raise ArgumentError.new("Missing key :param") if !ques.has_key? :param
				raise ArgumentError.new("Missing key :q") if !ques.has_key? :q
				
				qs.clear
				if(!ques[:desc].nil?)							# If there is a :desc key,
					if(ques[:desc].class == Array)					# If it is an array,
						ques[:desc].each {|d| qs << d}					# add each element to the text to display.
					else
						qs << ques[:desc]							# otherwise add :desc to text to display.
					end
				end
				qs << ques[:q]									# Add the actual question to text to display.
				
				@answers[ques[:param]] = getInput(qs)			# Ask the question and store the answer.
			end
			
			@answers		# Return the answers hash for easy usage.
		end
		
		
	end # class ConsoleIO
	

end # module KtCommon
