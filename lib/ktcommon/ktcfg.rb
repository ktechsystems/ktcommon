######################################################################################
#
# ktCfg.rb
#
# Jeff McAffee   10/24/09
#
# Purpose: YAML based config file related functions
#
######################################################################################

module KtCfg

  require 'yaml'
  require 'fileutils'
  require 'ktcommon/ktpath'
  
  class CfgFile
    
    attr_reader :rootDir

    
    def initialize(rootDir = nil)
      $LOG.debug "CfgFile::initialize"
      @rootDir = rootDir
      
      if( rootDir )
          @rootDir = File.rubypath(@rootDir)
          if( !File.exists?(@rootDir))
            $LOG.debug "Creating directory: #{@rootDir}"
            FileUtils.mkdir(@rootDir)
          end
      end
    end # initialize
    
    
    def rootDir=(dir)
      @rootDir = File.rubypath(dir)
    end
    
    
	def cfgFilePath(filename)
      $LOG.debug "CfgFile::cfgFilePath( #{filename} )"
	  filepath = filename
      if( @rootDir )
          filepath = File.join(@rootDir, filename)
      end
		  $LOG.debug "Filepath: #{filepath}"
		filepath
	end


    def write(filename, cfg)
      $LOG.debug "CfgFile::write"
      $LOG.debug "Config File Contents: #{cfg.inspect} "
      
      filepath = cfgFilePath(filename)
      
      open(filepath, 'w') { |f| YAML.dump(cfg, f) }
      
    end #write


    def read(filename)
          $LOG.debug "CfgFile::read"

      filepath = cfgFilePath(filename)
      
      cfg = {}
      
	  if(File.exists?(filepath))
		open(filepath) { |f| cfg = YAML.load(f) }
	  end
      cfg
    end
    
    
  end # class CfgFile


  
end # module KtPath



