######################################################################################
#
# ktPath.rb
#
# Jeff McAffee   6/27/09
#
# Purpose: File path related functions
#
######################################################################################


# Add path conversion functionality to File class
class File
end

class << File
    # Convert windows file path seperators to forward slashes.
    # path:: path to convert
    def rubypath path
      return path.gsub(/\\/, "/")
    end
	
    # Convert forward slashes to windows file path seperators (back slashes).
    # path:: path to convert
    def winpath path
      return path.gsub(/\//, "\\")
    end
end



