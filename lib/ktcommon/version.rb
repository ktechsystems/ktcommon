module KtCommon
  VERSION = "0.0.7" unless constants.include?("VERSION")
  APPNAME = "KtCommon" unless constants.include?("APPNAME")
  COPYRIGHT = "Copyright (c) 2013, kTech Systems LLC. All rights reserved" unless constants.include?("COPYRIGHT")
end
