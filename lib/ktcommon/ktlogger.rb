##############################################################################
# File:: ktLogger.rb
# Purpose: Set up logging for ruby applications
# 
# Author::    Jeff McAffee 10/30/2009
# Copyright:: Copyright (c) 2009 kTech Systems LLC. All rights reserved.
# Website::   http://ktechsystems.com
##############################################################################


require 'logger'

# Available release levels.
releaseLevels = [:debug, :alpha, :beta, :production]

$DEBUG          ||= false
$LOGGING        ||= false
$RELEASE_LEVEL  ||= :production

PROJNAME = 'ktLogger' unless defined?(PROJNAME)

if($LOGGING == true && PROJNAME.empty?)
  raise("ktLogger: PROJNAME needs to be defined for logging purposes.")
end

if(!releaseLevels.include? $RELEASE_LEVEL)
  raise("ktLogger: $RELEASE_LEVEL needs to be defined for logging purposes.")
end

debugfileindicator = "debug.txt"
if(File.exists?("/#{debugfileindicator}"))
  $DEBUG = true
  $LOGGING = true
  $RELEASE_LEVEL = :debug
end


if($LOGGING)
  $LOG = Logger.new( "#{PROJNAME.downcase}.log", 3, 100 * 1024 )     # Rotate 3 logs, each no larger than 100k
  $LOG.datetime_format = "%Y-%m-%d %H:%M:%S"
  $LOG.level = Logger::WARN
else
  $LOG = Logger.new(STDERR)
  $LOG.datetime_format = "%Y-%m-%d %H:%M:%S"
  $LOG.level = Logger::FATAL
end

if($RELEASE_LEVEL == :debug)
  $LOG.level = Logger::DEBUG
end

if($RELEASE_LEVEL == :alpha)
  $LOG.level = Logger::INFO
end

if($RELEASE_LEVEL == :beta)
  $LOG.level = Logger::WARN
end

if($RELEASE_LEVEL == :production)
  $LOG.level = Logger::ERROR
end

