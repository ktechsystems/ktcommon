######################################################################################
#
# ktCmdLine.rb
#
# Jeff McAffee   10/26/09
#
# Purpose: Command Line helper methods
#
######################################################################################

module KtCmdLine

#  require 'yaml'
#  require 'fileutils'
#  require 'ktcommon/ktpath'
  
  ###
  # Display a short message and collect input from the keyboard.
  #  Accepts either an array of strings or a single string.
  #  When the last array element is printed the cursor will 
  #  stay on the same line waiting for user input.
  #
  # text:: string or array[string]: text to display to user
  #
  def getInput(text)
      lines = text
	  *lines = text if text.class != Array
	  text = lines.pop
	  lines.each {|l| puts l }
	  $stdout.write "#{text} "
      i = $stdin.readline
      i.chomp!.strip!
      return i
  end # def getInput

    

  
end # module KtCmdLine



