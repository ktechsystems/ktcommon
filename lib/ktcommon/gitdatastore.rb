##############################################################################
# File:: 	gitdatastore.rb
# Purpose:: GitDataStore object encapsulates a Git repository for programatic
#           access.
# 
# Author::    Jeff McAffee 09/17/2010
# Copyright:: Copyright (c) 2010, kTech Systems LLC. All rights reserved.
# Website::   http://ktechsystems.com
##############################################################################

require 'stringio'
require 'git_store'

module KtCommon
	  
  ###########################################################################
  # GitDataStore encapsulates a Git repository
	class GitDataStore
		
		
		def initialize(location)
			$LOG.debug "GitDataStore::new"
			@location 	= location
			@store 		= nil
		end
		
		
		def ymlPath(path)
			#puts "IN: #{path}"
			raise ArgumentError.new("Empty path") if (path.nil? || path.empty?)
			
			path += ".yml" unless !File.extname(path).empty?
			#puts "OUT: #{path}"
			return path
		end
		
		
		def store()
			return @store unless @store.nil?
			raise ArgumentError.new("No repo location provided before store requested.") unless (!@location.nil? && !@location.empty?)
			begin
				@store = GitStore.new(@location)
			rescue ArgumentError => e
				if(e.message.include?("must be a valid Git repo"))
					raise RuntimeError.new("Cannot find a valid Git repo. Are you sure you've initialized the dir at #{@location}?")
				end
				raise $:
			end
		end
		
		
		def location(location)
			raise ArgumentError.new("Repo already opened when new location submitted: #{location}") unless @store.nil?
			@location = location
		end
		
		
		def create(path, data)
			$LOG.debug "GitDataStore::create"
			
			store[ymlPath(path)] = data
			commitMsg	= "Added to data store:  #{ymlPath(path)}"
			store.commit commitMsg
		end
		
		
		###
		# Read a specific file from the store based on a <em>path</em>.
		# path:: path to retrieve data from
		# returns:: <em>one(1)</em> data object
		def read(path)
			$LOG.debug "GitDataStore::read(#{ymlPath(path)})"
			
			return store[ymlPath(path)]
		end
		
		
		###
		# Read data from the store based on a <em>path</em>.
		# path:: path to retrieve data from
		# returns:: a Hash of data
		def read_path(path)
			$LOG.debug "GitDataStore::read_path(#{path})"
			
			return store[path]
		end
		
		
		def update(path, data)
			$LOG.debug "GitDataStore::update"
			
			store[ymlPath(path)] = data
			commitMsg	= "Updated data store: #{ymlPath(path)}"
			store.commit commitMsg
		end
		
		
		def delete(path)
			$LOG.debug "GitDataStore::delete"
			
			
			result 		= store.delete ymlPath(path)
			commitMsg	= "Deleted from data store: #{ymlPath(path)}"
			store.commit commitMsg
			
			return result
		end
		
		
	end # class GitDataStore
	
=begin ======================================================================
#      ======================================================================
=end
	


end # module KtCommon
