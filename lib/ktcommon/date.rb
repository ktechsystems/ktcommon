##############################################################################
# File:: 	date.rb
# Purpose:: KtCommon::Date module. Helper functions for date and time.
# 
# Author::    Jeff McAffee 10/06/2010
# Copyright:: Copyright (c) 2010, kTech Systems LLC. All rights reserved.
# Website::   http://ktechsystems.com
##############################################################################


##############################################################################
# 
module KtCommon


	module Date

		###
    # Return a timestamp string including both date and time.
    #
    # returns:: timestamp string
    #
    def timestamp()
			now = DateTime::now()
			now.strftime("%m/%d/%Y %H:%M:%S")
		end # def
		

		###
    # Return a datestamp string including only the date.
    #
    # returns:: datestamp string
    #
		def datestamp()
			now = Date::today()
			now.strftime("%m/%d/%Y")
		end # def
		

		###
    # Alias for #timestamp
    #
    def current_datetime()
				return timestamp()
		end # def


		###
    # Alias for #datestamp
    #
		def current_date()
				return datestamp()
		end # def

		
	end # module Date
	

end # module KtCommon
